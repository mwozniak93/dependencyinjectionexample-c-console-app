﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Car
    {
        INotifier _notifier;
        public void RegisterACar() { 
            Console.WriteLine("Car has been registred. and will send a notification after that");
            _notifier.SendNotification();
        }
         

         public Car(INotifier notifier)
         {
            _notifier = notifier;
        }

    }
}
