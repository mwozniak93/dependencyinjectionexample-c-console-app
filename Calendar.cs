﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Calendar
    {
        INotifier _notifier;

        public Calendar(INotifier notifier) {
            _notifier = notifier;
        }

        public void CreateEvent() {
            Console.WriteLine("Creating an event. and will send a notification after that");
            _notifier.SendNotification();
        
        }

    }
}
