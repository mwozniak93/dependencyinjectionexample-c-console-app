﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class Program
    {
        static void Main(string[] args)
        {
            Car newCar = new Car(new MailSender());
            Calendar newCalendarEvent = new Calendar(new TwitterNotifier());

            newCalendarEvent.CreateEvent();
            newCar.RegisterACar();

            Console.ReadKey();
        }
    }
}
