﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication5
{
    class TwitterNotifier :INotifier
    {
        public void SendNotification()
        {
            Console.WriteLine("Sending a Twitter notification!");
        }
    }
}
